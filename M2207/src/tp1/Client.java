package tp1;

public class Client {
	
	String n,p;
	
	private Compte compteCourant;
	

		public Client(String n,String p,Compte compte) {
			
			this.n = n;
			this.p = p;
			this.compteCourant = compte;
		}
		
		public String GetNom() {
			return n;
		}
		
		public String GetPrenom() {
			return p;
		}
	
		
		public double afficherSolde() {
			
			System.out.println(this.compteCourant.getSolde() );
			
			return 0;
		}
		
		public void depot(int n) {
		 this.compteCourant.depot(n); }
		
		
}
