package tp1;

public class Compte {
	
	private int numero,solde;
	private double decouvert;
	
	
	public Compte(int numero) {
		
		solde = 0;
		decouvert = 0;
		
	}
	
	public void setDecouvert(double montant) {
		
		decouvert += montant;
		
	}
	
    public double getDecouvert() {
		
		return decouvert;
		
    }
	
    public int getNumero() {
		
		return numero;
		
    }
    
    public double getSolde() {
		
		return solde;
		
    }
    
    public void afficherSolde() {
    	
    	System.out.println("Solde = " + solde);
    	
    }
 
    public void depot(double montant) {
    	
    	solde += montant;
    	
    }
    
    public String retrait(int montant) {
    	
    	if( montant < solde+decouvert ) {
    		
    		solde -= montant;
    		
    		return "Retrait effectu�.";
    		
    	}
    		
    	else {
    		
    		return "Retrait refus�.";
    		
    	} 
    		
    				
    } 
		
    	

}
    


