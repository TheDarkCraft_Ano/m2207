package td3;

public class TestAnimal extends Animal {
	
	public static void main(String[] args) {
		
		Animal animal = new Animal();
		
		System.out.println("Pour Animal:");
		
		System.out.println(animal.toString());
		
		System.out.println(animal.affiche());
		
		System.out.println(animal.cri());
		
		// Mon chien peut faire affiche et cri car il a h�riter de Animal
		
		Chien chien = new Chien();
		
		System.out.println("Pour Chien:");
		
		// 2.2 Mon chien Affiche et cri en r�cup�rant les info dans la class m�re Animal
		
		System.out.println(chien.affiche());
		
		// Quand on passe Cri en private le cri ne fonctionne plus car le cri est devenue priv�
		
		/* 2.4 le chien Affiche "Je suis un chien" et ne dit plus " je suis un animal"
		 * car l'ordre de recherche de la m�thide par de la class Chien et si il n'y a rien
		 * on vas chercher dans la class m�re Animal */
		
		// 2.5 Mon chien Cri
		
		System.out.println(chien.cri());
		
		//  Le chat
		
		Chat chat = new Chat();
		
		System.out.println("Pour Chat:");
		
        System.out.println(chat.affiche());
		
		System.out.println(chat.cri());
		
		System.out.println(chat.miauler());
		
		// 3.4 Test tout : 
		System.out.println("Question 3.4 :");
		
		System.out.println(animal.cri());
		
		System.out.println(chien.cri());
		
		System.out.println(chat.cri());
		
		System.out.println(chat.miauler());
		
		//Animal ne sait pas miauler seul les chat le peuvent ici 
		
		System.out.println(" Animal de sait pas miauler ! ");
		
	 // System.out.println(animal.miauler());
		
		
		
	}
}
