package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MonAppliGraphique extends JFrame {

	// Attributs

	JButton b1,b,b2,b3,b4,b5;
	JLabel monLabel;
	JTextField monTextField;


	//Constructeurs

	public MonAppliGraphique () {
		super();
		this.setTitle("Choisit un pays !");
		this.setSize(800,400);
		this.setLocation(960,350); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);



		/*
		 * ON JOUE ICI
		 */

		//Declaration
		b1 = new JButton("Russia");
		b2 = new JButton("Japan");
		b3 = new JButton("France");
		b4 = new JButton("U.S.A");
		b5 = new JButton("Mexico");
		monLabel = new JLabel("Je suis un label");
		monTextField = new JTextField("Je suis un TextField");

		
		//Container

		Container panneau = getContentPane();
		
		panneau.setLayout(new GridLayout(2,3));
		
		panneau.add(b1, BorderLayout.NORTH);
		panneau.add(b2, BorderLayout.WEST);
		panneau.add(b3, BorderLayout.SOUTH);
		panneau.add(b4, BorderLayout.EAST);
		panneau.add(b5, BorderLayout.CENTER);
		//panneau.add(monLabel);
		//panneau.add(monTextField);
		
		


		/*
		 * FIN DE ZONE DE JEU
		 */


		System.out.println("La fen�tre est cr��e !");
		this.setVisible(true);
		// Toujours � la fin du constructeur
	}

	//M�thode Main

	public static void main(String[] args) {
		MonAppliGraphique app = new MonAppliGraphique () ;
	}

	// M�thodes






}


