package tp5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.*;

public class CompteurDeClic extends JFrame implements ActionListener {

	
		
		//Attribut
		JButton b1;
		JLabel l1;
		int nbclic = 0;
		
		//Construteur
		
		public CompteurDeClic(){
		super();
		this.setTitle("Compteur de clic");
		//Je met le double pour la taille des fen�tre car sur un �cran FullHD elle sont minuscule
		this.setSize(400,200);
		this.setLocation(960,350); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/*
		 * DEBUT DE ZONE DE JEU
		 */
		
		//Declaration
		b1 = new JButton("Click !!");
		l1 = new JLabel("Tu a clicker " + nbclic + " fois");
		
		Container panneau = getContentPane();
		
		panneau.setLayout(new GridLayout(2,1));
		
		panneau.add(b1, BorderLayout.NORTH);
		b1.addActionListener(this);
		panneau.add(l1, BorderLayout.CENTER);
		
		/*
		 * FIN DE ZONE DE JEU
		 */
		
		System.out.println("La fen�tre est cr��e !");
		this.setVisible(true);
		}
		
		//Methodes
		
		public void actionPerformed(ActionEvent e) { 
			
		nbclic ++;
		l1.setText("Tu a clicker " + nbclic + " fois");
		System.out.println("Une action a �t� d�tect�e");
		
		}
		
		
		//Methodes Main	
		public static void main(String[] args) {
			CompteurDeClic app = new CompteurDeClic () ;
		}

}
