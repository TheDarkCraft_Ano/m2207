package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		Humain remi = new Humain( "R�mi" );
		
		remi.sePresenter();
		remi.boire();
		
		System.out.println("");
		System.out.println("Pr�sentation : ");
		System.out.println("");
		
		Dame limane = new Dame( "Limane" );
		limane.sePresenter();
		limane.priseEnOtage();
		limane.estLiberee();
		
		Brigand prescilla = new Brigand("Prescilla");
		
		prescilla.sePresenter();
		
		Cowboy hyvong = new Cowboy( "Hy-vong" );
		
		hyvong.sePresenter();
		
		System.out.println("");
		System.out.println("KIDNAPPING !! : ");
		System.out.println("");
		
		limane.sePresenter();
		prescilla.sePresenter();
		prescilla.enleve(limane);
		hyvong.tire(prescilla);
		hyvong.libere(limane);
		limane.sePresenter();
		System.out.println("");
		hyvong.sePresenter();
		prescilla.sePresenter();
		
		System.out.println("");
		System.out.println("Sh�rif : ");
		System.out.println("");
		
		Sherif lewis = new Sherif("Lewis");
		
		lewis.sePresenter();
		
		System.out.println("");
		
		lewis.coffrer(prescilla);
		
		System.out.println("");
		
		lewis.sePresenter();
		
		System.out.println("");
		System.out.println("Exo 9 :");
		System.out.println("");
		// Exo 9 
		
		Cowboy hita = new Sherif("Hitachi");
		
		hita.sePresenter();
		hita.tire(prescilla);
		hita.boire();
		
		/* On ne peut pas lui demander de coffrer un briganf car c'est 
		 * un cowboy , mais il h�rite tout de m�me des attribut du 
		 * sh�rif.
		 */
		

	}

}
