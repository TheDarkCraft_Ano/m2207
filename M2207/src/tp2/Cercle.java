package tp2;

public class Cercle extends Forme{
	
	//Attributs
	
	private double rayon;
	
	//Constructeurs
	
	public Cercle() { 
		super(); 
		rayon = 1.0; 
	}
	public Cercle(double r) {
		rayon = r;

	}
	public Cercle(double r,String c, boolean co) {
		super(c,co);
		rayon = r;

	}
	//Accessseurs
	
	public void setRayon(double r) { 
		rayon = r;
	}

	public double getRayon() {
		return rayon; 
	}
	//M�thodes
	
	public String seDecrire() { 

		return "Un cercle de rayon " + rayon + " issue d'" + super.seDecrire();
	}

	public double calculerAire() {
		double res = Math.PI*(rayon*rayon);
		return res;
	}

	public double calculerPerimetre() {
		double res = 2*Math.PI*rayon;
		return res;
	}

}
