package tp2;

public class Forme {
	
	//Attributs
	
	private String couleur;
	private boolean coloriage;
	public static int nombreF = 0;
	
	//Constructeurs
	
	public Forme() { 
		couleur = "orange";
		coloriage = true; 
		nombreF ++;
	}

	public Forme(String c, boolean r) {	
		couleur = c ;
		coloriage = r;
		nombreF ++;
	}
	
	//Accesseurs
	
	public String getCouleur() { 
		return couleur;
	}

	public void setCouleur(String c) { 
		couleur = c;
	}

	public boolean isColoriage() {
		return coloriage; 
	}

	public void setColoriage(boolean b) { 
		coloriage = b; 
	}
	
	public static int getNbFormeGenerer() {
		return nombreF;
	}
	
	//M�thodes
	
	public String seDecrire() {

		return "Une Forme de couleur " + couleur + " et de coloriage " + coloriage;
	}
	
}
