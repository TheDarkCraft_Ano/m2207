package tp2;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class TestForme {

	public static void main(String[] args) {

		DecimalFormat df = new DecimalFormat();
		df.setRoundingMode(RoundingMode.HALF_UP);
		
		System.out.println("Le nombre actuelle de forme(s) cr��e(s) est de : " + Forme.getNbFormeGenerer());
		
		Forme f1 = new Forme();
		Forme f2 = new Forme( "Vert" , false);

		

		System.out.println(" f1 : " +  f1.getCouleur() + "  -  " + f1.isColoriage() );

		System.out.println(" f2 : " +  f2.getCouleur() + "  -  " + f2.isColoriage() );

		// 1.5 Modification de f1
		System.out.println("");
		System.out.println("Modification de f1");
		System.out.println("");

		f1.setCouleur("rouge");
		f1.setColoriage(true);

		System.out.println(" f1 : " +  f1.getCouleur() + "  -  " + f1.isColoriage() );

		// 1.7 Test de se d�crire

		System.out.println("");
		System.out.println("Test de seDecrire :");
		System.out.println("");

		System.out.println("f1: ");
		System.out.println(f1.seDecrire());

		System.out.println("f2: ");
		System.out.println(f2.seDecrire());

		// 2.4 Ajout du cercle C1

		Cercle c1 = new Cercle();

		System.out.println("c1: ");

		System.out.println(c1.seDecrire());

		//2.8 Cr�ation cercle c2

		Cercle c2 = new Cercle(2.5);

		System.out.println("c2: ");

		System.out.println(c2.seDecrire());

		Cercle c3 = new Cercle(3.2, "jaune", false);

		System.out.println("c3: ");

		System.out.println(c3.seDecrire());

		// Test d'Aire et de p�rim�tre sur C2 et C3

		System.out.println("c2: ");

		System.out.println("Aire = " + df.format(c2.calculerAire()) + "cm�");
		System.out.println("P�rim�tre = " + df.format(c2.calculerPerimetre()) + "cm");

		System.out.println("c3: ");

		System.out.println("Aire = " + df.format(c3.calculerAire()) + "cm�");
		System.out.println("P�rim�tre = " + df.format(c3.calculerPerimetre()) + "cm");

		//3.2 Cr�ation et test cylindre

		Cylindre cy1 = new Cylindre();

		System.out.println("cy1 : ");

		System.out.println(cy1.seDecrire());

		// 3.4 Cr�ation et test d'un cylindre avec 4 attributs

		Cylindre cy2 = new Cylindre( 4.2 , 1.3 ,"bleu" , true);

		System.out.println("cy2 : ");

		System.out.println(cy2.seDecrire());

		// 3.5 Affichage calcul  Volume

		System.out.println("Volume de : ");

		System.out.println("cy1 : " + df.format(cy1.calculerVolume()) + "cm�");

		System.out.println("cy2 : " + df.format(cy2.calculerVolume()) + "cm�");
		
		System.out.println("Le nombre actuelle de forme(s) cr��e(s) est de : " + Forme.getNbFormeGenerer());

	}

}
