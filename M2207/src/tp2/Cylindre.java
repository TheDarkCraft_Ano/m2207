package tp2;

public class Cylindre extends Cercle {
	
	//Attributs
	
	private double hauteur;
	
	//Constructeurs
	
	public Cylindre() {
		super();
		hauteur = 1;
	}
	public Cylindre(double h,double r,String c, boolean co) {
		super(r, c , co);
		hauteur = h;
	}
	
	//Accesseurs
	
	public void setHauteur(double h) {
		hauteur = h;
	}

	public double getHauteur() {
		return hauteur;
	}
	
	//M�thodes
	
	public String seDecrire() { 

		return "Un cylindre de hauteur " + hauteur + " issue d'" + super.seDecrire();
	}
	public double calculerVolume() {
		double res = super.calculerAire()*hauteur;
		return res;
	}
}
