package td1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SaisieClavier {
	
	public static void main(String[] args) {
		
		DecimalFormat df = new DecimalFormat("##.##" );
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Saisissez votre nom");
		
		String nom = sc.nextLine();
		
		System.out.println("Vous voulez additionner 2 nombres entre eux entrez le premier nombre: ");
		
		String val1 = sc.nextLine();
		
		System.out.println("Entrez le deuxiemme nombre: ");
		
		String val2 = sc.nextLine();
		
		//convertion de string a integer
		int val1int;
		float val2f;
		
		val1int = Integer.parseInt(val1);
		val2f = Float.parseFloat(val2);
		
		double val3 = val1int + val2f;
		
		
		
		System.out.println("Bonjour " + nom + " le r�sultat de " + val1 + " + " + val2 + " est : " + df.format(val3) );
		
		
	}

}
