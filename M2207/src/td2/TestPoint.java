package td2;

import java.util.Scanner;

public class TestPoint {

	public static void main(String[] args) {
// Nouveau Point
		Point p;
		p = new Point(7,2);
// Affichage des coordonn�es + ajout d'un scanner pour entrez ses propre valeur
		
		System.out.println("Les coordonn�es du point sont x = " + p.getX() + " y = " + p.getY() );
		
		System.out.println("entrez les nouvelles valeur de" );
		
		Scanner sc = new Scanner(System.in);
		
									System.out.print("x=");
									
									String val1 = sc.nextLine();
									
									System.out.print("y=");
									
									String val2 = sc.nextLine();
									
									int valx = Integer.parseInt(val1);
									int valy = Integer.parseInt(val2);
		
		p.setX(valx);
		p.setY(valy);
		
		System.out.println("Les nouvelles coordonn�es du point sont x = " + p.getX() + " y = " + p.getY() + " Par getX() , getY()" );

// Affichage en utilisant p.x au lieu de getX()
		
		System.out.println("Exercice 1.4");
		System.out.println("Acc�s par p.x , donc x = " + p.x);
		
// cr�ation d'un deuxiemme point p2		
		
		Point p2;
		
// initialiser a x=0 et y=0
		
		p2 = new Point(0,0);
		
		System.out.println("Coordonn�es de p2 sont, x= " + p2.x + " y= " + p2.y + " Exo 1.6");
		
// Scanner pour choisir les valeur de d�placement du point 2
		
		System.out.println("D�placer le de N pas en x et en y !");
		
									System.out.print("x=");
									
									String val3 = sc.nextLine();
									
									System.out.print("y=");
									
									String val4 = sc.nextLine();
									
									int valx1 = Integer.parseInt(val3);
									int valy2 = Integer.parseInt(val4);
									
// D�placement du point 2 avec la m�thode d�placer
									
		p2.deplace(valx1, valy2);
									
		System.out.println("Les nouvelles coordonn�es du point2 sont x = " + p2.getX() + " y = " + p2.getY() );	
		
		System.out.println("Je d�cide de r�initialiser mon point1 et mon point2 a des coordonn�s nul");
		
		p.reset();
		p2.reset();
		
		System.out.println("Voila maintennant voicie les coordonn�es du point 1 et 2 (p1 XY / p2 XY) p1 " + p.getX() + p.getY() + " / p2 " + p2.getX() + p2.getY() );
		
	}

}
