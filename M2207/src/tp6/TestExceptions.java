package tp6;

public class TestExceptions {

	public static void main(String[] args) {
		int x = 2, y = 0;
		//On avait un problemme d'execution du a une division par 0
		//On ne vois pas le "Fin de programme" dans la console 
		try{
			System.out.println("y/x = " + y/x);
			System.out.println("x/y = " + x/y);
			System.out.println("Commande de fermeture du programme");
			
		/*Je Constate que des qu'il y a une erreur on quitte le "try" pour 
			allez dans le catch et finir d'�x�cuter le code */
		}
		catch (Exception e){
			System.out.println("Une exeption a �t� captur�e");
		}
		System.out.println("Fin du programme");
		
	}

}
