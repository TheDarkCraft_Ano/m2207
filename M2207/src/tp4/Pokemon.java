package tp4;

public class Pokemon {
	// Attributs

	private int energie = 0;
	private int maxEnergie = 0;
	private String nom;
	private int puissance;

	// Constructeurs

	public Pokemon(String n) {

		nom = n;

		maxEnergie = 50 + (int) (Math.random() * ((90 - 50) + 1));

		energie = 30 + (int) (Math.random() * ((maxEnergie - 30) + 1));

		puissance = 3 + (int) (Math.random() * ((10 - 3) + 1));
	}

	// Accesseurs

	public String getNom() {

		return nom;

	}

	public int getEnergie() {

		return energie;

	}

	public int getPuissance() {

		return puissance;

	}
	// M�hodes

	// 1.3 le pokemon doit savoir se present�
	public void sePresenter() {

		System.out.println("Je suis " + getNom() + ", j'ai " + getEnergie() + " points d'energie (" + maxEnergie
				+ " max )" + " et une puissance de " + puissance);

	}

	public boolean isAlive() {

		if (energie == 0) {
			return false;
		} else {
			return true;
		}

	}

	public void manger() {

		int rand;
		int max = maxEnergie;
		int nouv;

		rand = 10 + (int) (Math.random() * ((30 - 10) + 1));

		nouv = rand + energie;

		if (nouv > maxEnergie) {

			nouv = maxEnergie;

		} else {
		}

		energie = nouv;

	}

	public void vivre() {
		int rand;
		int min = 0;
		int nouv;

		rand = 20 + (int) (Math.random() * ((40 - 20) + 1));

		nouv = energie - rand;

		if (nouv < min) {

			nouv = min;

		} else {
		}

		energie = nouv;

	}

	public void perdreEnergie(int perdre) {

		float percEn;

		percEn = ((float) energie / (float) maxEnergie) * 100;

		float perdFurie = (float) (perdre * 1.5);
		System.out.println(percEn);

		// Ajout du mode Furie ( Ayant eu un probl�me lors de l'imbrication des if , je
		// vais les s�parer en cascade. ( !!!!!!! ATTENTION CETTE PARTIE DU CODE NE
		// FONCTIONNE PLUS POUR LE MOMENT VOIR VERSION PRECEDENTE POUR LA TESTER

		if (energie - perdFurie < 0) {

			energie = 0;
		}

		else {

			energie -= perdFurie;

			System.out.println(perdFurie);
		}

		if (energie - perdre < 0) {
			energie = 0;
		} else {
			energie -= perdre;
		}
		if (energie < percEn) {
		} else {
		}

	}

	public void attaquer(Pokemon adversaire) {

		float percEn;

		percEn = ((float) energie / (float) maxEnergie) * 100;

		// Ajout du mode Furie
		if (energie < percEn) {
			adversaire.perdreEnergie((this.getPuissance() * 2));
		} else {
			adversaire.perdreEnergie(this.getPuissance());
		}
		/*
		 * Pour avoir un nombre al�atoire entre 0 ou 1 , je vais le faire entre 10 et 20
		 * et si j'obtient un resultat en dessous de 15 sa seras 0 et sinon sa seras 1
		 */

		int rand;
		int nouv;

		rand = 10 + (int) (Math.random() * ((20 - 10) + 1));

		if (rand < 15) {
			nouv = 0;
		} else {
			nouv = 1;
		}

		this.perdreAttaque(nouv);

	}

	private void perdreAttaque(int p) {

		if (puissance - p < 1) {

			puissance = 1;
		}

		else {

			puissance -= p;
		}

	}

}
