package tp4;

public class TestPokemon {

	public static void main(String[] args) {

		Pokemon jac = new Pokemon("Jaquie");

		System.out.println("Se pr�sente -> mange -> pr�sente -> vivre -> presenter -> vivre -> pr�sente");

		jac.sePresenter();

		jac.manger();

		jac.sePresenter();

		jac.vivre();

		jac.sePresenter();

		jac.vivre();

		jac.sePresenter();

		// Jouons avec la vie d'un pokemon pour voir sont nombre de cycles
		// 1.8

		Pokemon mit = new Pokemon("Mitchell");
		int cpt = 0;
		mit.sePresenter();
		while (mit.getEnergie() > 0) {

			mit.manger();
			mit.vivre();
			cpt++;

		}

		System.out.println(mit.getNom() + " a v�cu, heureux " + cpt + " cycles !");

	}

}
