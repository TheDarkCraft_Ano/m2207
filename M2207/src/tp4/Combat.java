package tp4;

public class Combat {

	public static void main(String[] args) {

		Pokemon nat = new Pokemon("Natacha");

		Pokemon mau = new Pokemon("Maurane");

		nat.sePresenter();

		mau.sePresenter();

		System.out.println("-----------------------");

		// Variable compteur round
		int cpt = 1;

		while (nat.getEnergie() > 0 && mau.getEnergie() > 0) {

			nat.attaquer(mau);

			mau.attaquer(nat);

			System.out.println(
					"Round " + cpt + " " + nat.getNom() + ": (en)" + nat.getEnergie() + " (atk)" + nat.getPuissance()
							+ " - " + mau.getNom() + ": (en)" + mau.getEnergie() + " (atk)" + mau.getPuissance());

			cpt++;
		}

		System.out.println("-----------------------");

		if (nat.getEnergie() != 0) {

			System.out.println(nat.getNom() + " gagne en " + (cpt - 1) + " rounds");

		} else if (mau.getEnergie() != 0) {

			System.out.println(mau.getNom() + " gagne en " + (cpt - 1) + " rounds");
		} else {

			System.out.println(nat.getNom() + " et " + mau.getNom() + " son a �galit�");

		}

	}

}
